var mainState = {
	preload: function(){
		game.load.image('ceiling','assets/ceiling.png');
		game.load.image('nails','assets/nails.png');
		game.load.image('normal','assets/normal.png');
		game.load.image('wall','assets/wall.png');
		//game.load.image('cube1','assets/cube.png');
		game.load.image('cube2','assets/cube2.png');
		game.load.image('cube3','assets/cube3.png');
		game.load.image('pixel2','assets/pixel2.png');
		game.load.image('pixel3','assets/pixel3.png');
		
		
		game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
		game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
		game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
		game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		
		game.load.audio('die', ['assets/sounds/die.wav','assets/sounds/die.mp3']);
		game.load.audio('step', ['assets/sounds/step.wav','assets/sounds/step.mp3']);
		game.load.audio('hurt', ['assets/sounds/hurt.wav','assets/sounds/hurt.mp3']);
		game.load.audio('bg', ['assets/sounds/main.mp3', 'assets/sounds/main.wav']);
		game.load.audio('heal', ['assets/sounds/heal.mp3', 'assets/sounds/heal.wav']);
		game.load.audio('upgrade', ['assets/sounds/upgrade.mp3', 'assets/sounds/upgrade.wav']);
	},
	create: function() {
		this.createBounders();
		this.createPlayer();
		this.createInitialPlatform();
		this.createTexts();
		this.hurtbyceiling = true;
		score = 0;
		this.time = game.time.now;
		this.extrahp = 0;
		
		this.emitter1 = game.add.emitter(0, 0, 15);
		this.emitter1.makeParticles('pixel2');
		this.emitter1.setYSpeed(-150, 150);
		this.emitter1.setXSpeed(-150, 150);
		this.emitter1.setScale(2, 0, 2, 0, 0);
		this.emitter1.gravity = 0;
		
		this.emitter2 = game.add.emitter(0, 0, 15);
		this.emitter2.makeParticles('pixel3');
		this.emitter2.setYSpeed(-150, 150);
		this.emitter2.setXSpeed(-150, 150);
		this.emitter2.setScale(2, 0, 2, 0, 0);
		this.emitter2.gravity = 0;
		
		this.dieSound = game.add.audio('die');
		this.hurtSound = game.add.audio('hurt');
		this.stepSound = game.add.audio('step');
		this.healSound = game.add.audio('heal');
		this.upgradeSound = game.add.audio('upgrade');
		
		keyboard = game.input.keyboard.addKeys({
			'left': Phaser.Keyboard.LEFT,
			'right': Phaser.Keyboard.RIGHT,
			'up': Phaser.Keyboard.UP,
			'shift': Phaser.Keyboard.SHIFT
		});
		
		bg1 = game.add.audio('bg');
		bg1.loop = true;
		bg1.volume = 0.4;
		bg1.play();
		bg2.stop();
		bg3.stop();
	},
	update: function() {
		game.physics.arcade.collide(player, [this.leftWall, this.rightWall]);
		game.physics.arcade.collide(player, platforms, this.effect.bind(this));
		game.physics.arcade.collide(player, cubes, this.effect2.bind(this));
		if (keyboard.left.isDown) {
			player.body.velocity.x = -200;
			if(player.touchOn == undefined){
				if(player.unbeatableTime > game.time.now)
					player.animations.play('leftDown_inv');
				else
					player.animations.play('leftDown');
			}
			else{
				if(player.unbeatableTime > game.time.now)
					player.animations.play('left_inv');
				else
					player.animations.play('left');
			}
			if(keyboard.shift.isDown)
				player.body.velocity.x = -350;
		}
		else if (keyboard.right.isDown) {
			player.body.velocity.x = 200;
			if(player.touchOn == undefined){
				if(player.unbeatableTime > game.time.now)
					player.animations.play('right_Down_inv');
				else
					player.animations.play('rightDown');
			}
			else{
				if(player.unbeatableTime > game.time.now)
					player.animations.play('right_inv');
				else
					player.animations.play('right');
			}
			if(keyboard.shift.isDown)
				player.body.velocity.x = 350;
		}
		else {
			player.body.velocity.x = 0;
			if(player.touchOn == undefined){
				if(player.unbeatableTime > game.time.now)
					player.animations.play('fall_inv');
				else
					player.animations.play('fall');
			}
			else{
				if(player.unbeatableTime > game.time.now)
					player.frame = 17;
				else
					player.frame = 8;
			}
		}
		
		if(keyboard.up.isDown && player.body.touching.down)
			player.body.velocity.y = -300;

		this.checkTouchCeiling();
		this.checkGameOver();
		if(player.body.touching.down == false){
			player.touchOn = undefined;
		}
		this.updatePlatforms();
		this.createPlatforms();
		if((game.time.now - this.time)>1000){
			score ++;
			this.time += 1000;
		}
		
		text.setText("生命 : "+player.life+' / '+ (12+this.extrahp));
		this.text2.setText("地下 "+score+" 層");
	},
	
	createBounders: function() {
		
		this.leftWall = game.add.sprite(0, 0, 'wall');
		game.physics.arcade.enable(this.leftWall);
		this.leftWall.body.immovable = true;

		this.rightWall = game.add.sprite(383, 0, 'wall');
		game.physics.arcade.enable(this.rightWall);
		this.rightWall.body.immovable = true;

		this.ceiling = game.add.sprite(15, 0, 'ceiling');
	},
	
	createPlayer: function() {
		player = game.add.sprite(200, 300, 'player');
		game.physics.arcade.enable(player);
		player.body.gravity.y = 500;
		player.animations.add('left', [0, 1, 2, 3], 6);
		player.animations.add('right', [9, 10, 11, 12], 6);
		
		player.animations.add('leftDown', [18, 19, 20, 21], 6);
		player.animations.add('rightDown', [27, 28, 29, 30], 6);
		player.animations.add('fall', [36, 37, 38, 39], 6);
		
		player.animations.add('left_inv', [4, 5, 6, 7], 6);
		player.animations.add('right_inv', [13, 14, 15, 16], 6);
		player.animations.add('leftDown_inv', [22, 23, 24, 25], 6);
		player.animations.add('right_Down_inv', [31, 32, 33, 34], 6);
		player.animations.add('fall_inv', [40, 41, 42, 43], 6);
		player.frame = 8;
		player.life = 12;
		player.touchOn = undefined;
		player.unbeatableTime = 0;
	},
	
	checkTouchCeiling: function() {
		if(player.body.y < 16) {
			player.body.velocity.y = 200;
			if(hurtbyceiling == true){
				hurtbyceiling = false;
				player.life -= 5;
				this.hurtSound.play();
			}
			if(game.time.now > player.unbeatableTime) {
				player.unbeatableTime = game.time.now + 1000;
			}
			if(player.body.touching.down){
				player.body.checkCollision.down = false;
				setTimeout(function(){
					player.body.checkCollision.down = true;
				},50);
			}
		}
		else
			hurtbyceiling = true;
	},
	
	checkGameOver: function() {
		if(player.body.y > 450 || player.life <= 0) {
			this.gameOver();
		}
	},
	
	gameOver: function() {
		platforms.forEach(function(i) {i.destroy()});
		platforms = [];
		cubes.forEach(function(i) {i.destroy()});
		cubes = [];
		this.dieSound.play();
		game.state.start('over');
	},
	
	createPlatforms: function() {
		
		if(game.time.now > lastTime + 700) {
			var rand = Math.random() * 100;
			if(rand < 7){
				lastTime = game.time.now;
				this.createOnePlatform();
			}
		}
	},
	
	createOnePlatform: function() {

		var platform;
		var x = Math.random() * (400 - 96 - 40) + 20;
		var rand = Math.random() * 100;
		var rand2 = Math.random() *100;
		var cube;

		if (rand < 20-score/20.0) {
			platform = game.add.sprite(x, 400, 'normal');
		} else if (rand < 30-score/20.0) {
			platform = game.add.sprite(x, 400, 'conveyorLeft');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		} else if (rand < 40-score/20.0) {
			platform = game.add.sprite(x, 400, 'conveyorRight');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		} else if (rand < 60-score/20.0) {
			platform = game.add.sprite(x, 400, 'trampoline');
			platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
			platform.frame = 3;
		} else if (rand < 80+score/20.0) {
			platform = game.add.sprite(x, 400, 'nails');
			game.physics.arcade.enable(platform);
			platform.body.setSize(96, 15, 0, 15);
		} else {
			platform = game.add.sprite(x, 400, 'fake');
			platform.animations.add('turn', [1, 2, 3, 4, 5, 0], 8);
		}
		if(rand2 < 15){
			cube = game.add.sprite(x+30 , 370, 'cube2');
			game.physics.arcade.enable(cube);
			cube.body.immovable = true;
			cubes.push(cube);
		}
		else if(rand2 < 20){
			cube = game.add.sprite(x+30 , 370, 'cube3');
			game.physics.arcade.enable(cube);
			cube.body.immovable = true;
			cubes.push(cube);
		}

		game.physics.arcade.enable(platform);
		platform.body.immovable = true;
		platforms.push(platform);

		platform.body.checkCollision.down = false;
		platform.body.checkCollision.left = false;
		platform.body.checkCollision.right = false;
	},
	
	updatePlatforms: function() {
		for(var i=0; i<platforms.length; i++) {
			platforms[i].body.y -= 2;

			if(platforms[i].body.y < 0) {
				platforms[i].destroy();
				platforms.splice(i, 1);
			}
		}
		for(var i=0; i<cubes.length; i++) {
			cubes[i].body.y -= 2;

			if(cubes[i].body.y < 0) {
				cubes[i].destroy();
				cubes.splice(i, 1);
			}
		}
		
	},
	
	effect: function(player, platform) {        //player和platform collide時的效果
		if(player.touchOn !== platform && player.body.touching.down)
			this.stepSound.play();
		if(platform.key == 'conveyorLeft') {
			player.body.x -= 2;
			if(player.touchOn !== platform){
				player.touchOn = platform;
				if(player.life < 12+this.extrahp && player.body.touching.down)
					player.life ++;
			}
		}
		if(platform.key == 'conveyorRight') {
			player.body.x += 2;
			if(player.touchOn !== platform){
				player.touchOn = platform;
				if(player.life < 12+this.extrahp && player.body.touching.down)
					player.life ++;
			}
		}
		if(platform.key == 'trampoline') {
			player.body.velocity.y = -260;
			platform.play('jump');
			if(player.touchOn !== platform){
				player.touchOn = platform;
				if(player.life < 12+this.extrahp && player.body.touching.down)
					player.life ++;
			}
		}
		if(platform.key == 'nails') {
			if(player.touchOn !== platform) {
				player.life -= 5;
				this.hurtSound.play();
				player.touchOn = platform;
				if(player.unbeatableTime < game.time.now){
					player.unbeatableTime = game.time.now + 1000;
				}	
			}
		}
		if(platform.key == 'normal') {
			if(player.touchOn !== platform) {
				if(player.life < 12+this.extrahp && player.body.touching.down) {
					player.life += 1;
				}
				player.touchOn = platform;
			}
		}
		if(platform.key == 'fake') {
			if(player.touchOn !== platform) {
				setTimeout(function() {
					platform.play('turn');
					platform.body.checkCollision.up = false;
				}, 200);
				player.touchOn = platform;
				if(player.life < 12+this.extrahp && player.body.touching.down)
					player.life ++;
			}
		}
	},
	
	createInitialPlatform: function(){
		var platform;
		platform = game.add.sprite(150, 350, 'normal');
		game.physics.arcade.enable(platform);
		platform.body.immovable = true;
		platforms.push(platform);

		platform.body.checkCollision.down = false;
		platform.body.checkCollision.left = false;
		platform.body.checkCollision.right = false;
	},
	
	createTexts: function() {
		var style = {fill: '#66a3ff', fontSize: '30px'}
		text = game.add.text(410, 160, '', style);
		this.text2 = game.add.text(410, 200, '', style);
	},
	effect2: function(player, cube) {
		for(var i=0; i<cubes.length; i++) {
			if(cubes[i] === cube) {
				if(cube.key == 'cube2'){
					this.upgradeSound.play();
					if(player.life < 30)
						player.life++;
					if(this.extrahp < 18)
						this.extrahp++;
					this.emitter1.x = cube.x;
					this.emitter1.y = cube.y;
					this.emitter1.start(true, 800, null, 15);
				}
				else if(cube.key == 'cube3'){
					this.healSound.play();
					player.life = 12+this.extrahp;
					this.emitter2.x = cube.x;
					this.emitter2.y = cube.y;
					this.emitter2.start(true, 800, null, 15);
				}
				cubes[i].destroy();
				cubes.splice(i, 1);
			}
		}
	}
};

var menuState = {
	preload: function() {
		game.load.image('wall','assets/wall.png');
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		game.load.audio('bg', ['assets/sounds/menu.mp3', 'assets/sounds/menu.wav']);
	},
	create: function() {
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
		
		var title = game.add.text( 50, -50, '小朋友下樓梯', { font: '50px Arial', fill: '#66a3ff' });
		game.add.tween(title).to({y: 80},1000).easing(Phaser.Easing.Bounce.Out).start();
		
		this.createPlayer();
		
		var hint = game.add.text( 90, 200, '按下方向鍵開始遊戲', { font: '25px Arial', fill: '#66a3ff' });
		game.add.tween(hint).to({alpha: 0}, 700).to({alpha: 1}, 700).to({alpha: 1}, 700).loop().start();
		
		var scoreBoard = game.add.text( 450, 30, '排行榜', { font: '30px Arial', fill: '#66a3ff' });
		keyboard = game.input.keyboard.createCursorKeys();
		
		
		var query = firebase.database().ref('scoreBoard').orderByChild('score').limitToLast(5);
		var count = 0;
		query.on('child_added', function(snap) {
		var record = snap.val();
		if(count == 4)
			var recordd = game.add.text( 420, 200-count*30, record.name + ' : ' + record.score, { font: '25px Arial', fill: '#ffff14' });
		else
			var recordd = game.add.text( 420, 200-count*30, record.name + ' : ' + record.score, { font: '25px Arial', fill: '#66a3ff' });
		  count++;
		});
		
		bg2 = game.add.audio('bg');
		bg2.loop = true;
		bg2.play();
		bg1.stop();
		bg3.stop();
		
		
	},
	update: function() {
		if(keyboard.left.isDown || keyboard.up.isDown || keyboard.down.isDown || keyboard.right.isDown)
			game.state.start('main');
	},
	
	createPlayer: function() {
		this.player1 = game.add.sprite(190, 300, 'player');
		this.player2 = game.add.sprite(90, 300, 'player');
		this.player3 = game.add.sprite(290, 300, 'player');
		
		this.player2.animations.add('leftDown', [18, 19, 20, 21], 6, true);
		this.player3.animations.add('rightDown', [27, 28, 29, 30], 6, true);
		this.player1.animations.add('fall', [36, 37, 38, 39], 6, true);
		
		this.player1.animations.play('fall');
		this.player2.animations.play('leftDown');
		this.player3.animations.play('rightDown');
	}
};

var overState = {
	preload: function() {
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		game.load.audio('bg', ['assets/sounds/over.mp3', 'assets/sounds/over.wav']);
	},
	create: function() {
		var text = game.add.text( 90, 100, 'RRRRRRRRRRRRRR~', { font: '40px Arial', fill: '#66a3ff' });
		
		this.player1 = game.add.sprite(10, 0, 'player');
		this.player2 = game.add.sprite(560, 0, 'player');
		this.player1.animations.add('fall', [36, 37, 38, 39], 6, true);
		this.player2.animations.add('fall', [36, 37, 38, 39], 6, true);
		this.player1.animations.play('fall');
		this.player2.animations.play('fall');
		game.physics.arcade.enable(this.player1);
		this.player1.body.gravity.y = 500;
		game.physics.arcade.enable(this.player2);
		this.player2.body.gravity.y = 500;
		
		var hint = game.add.text( 200, 250, '按下R鍵再來一次', { font: '25px Arial', fill: '#66a3ff' });
		game.add.tween(hint).to({alpha: 0}, 700).to({alpha: 1}, 700).to({alpha: 1}, 700).loop().start();
		var hint2 = game.add.text( 200, 300, '按下H鍵回到主頁', { font: '25px Arial', fill: '#66a3ff' });
		game.add.tween(hint2).to({alpha: 0}, 700).to({alpha: 1}, 700).to({alpha: 1}, 700).loop().start();
		
		keyboard = game.input.keyboard.addKeys({
			'H': Phaser.Keyboard.H,
			'R': Phaser.Keyboard.R,
		});
		
		bg3 = game.add.audio('bg');
		bg3.loop = true;
		bg3.play();
		bg1.stop();
		bg2.stop();
		
		var input = game.add.text( 240, 150, '地下 : '+score+' 層', { font: '25px Arial', fill: '#66a3ff' });
		var input = game.add.text( 150, 200, '你的名字 : ', { font: '25px Arial', fill: '#66a3ff' });
		game.add.plugin(PhaserInput.Plugin);
		input2 = game.add.inputField(280, 195,{
			font: '18px Arial',
			fill: '#66a3ff',
			fontWeight: 'bold',
			width: 150,
			padding: 8,
			borderWidth: 1,
			borderColor: '#000',
			borderRadius: 6,
			placeHolder: 'Name'
		});
	},
	update: function() {
		if(this.player1.body.y > 400){
			this.player1.body.y = 0;
			this.player1.body.velocity.y = 0;
		}
		if(this.player2.body.y > 400)
			this.player2.body.y = 0;
		if(keyboard.H.isDown){
			game.state.start('menu');
			var newrecordref = firebase.database().ref('scoreBoard').push();
            newrecordref.set({
                name: input2.value,
				score: score
			});
		}
		else if(keyboard.R.isDown){
			game.state.start('main');
			var newrecordref = firebase.database().ref('scoreBoard').push();
            newrecordref.set({
                name: input2.value,
				score: score
			});
		}
	}
};

var game = new Phaser.Game(600, 400, Phaser.AUTO, 'canvas');
var keyboard;
var platforms = [];
var lastTime = 0;
var text;
var player;
var bg1,bg2,bg3;
var score;
var input2;
var cubes = [];

game.state.add('main', mainState);
game.state.add('menu', menuState);
game.state.add('over', overState);
game.state.start('menu');